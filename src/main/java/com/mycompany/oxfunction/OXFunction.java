/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxfunction;

import com.sun.source.tree.ContinueTree;
import java.util.Scanner;

/**
 *
 * @author Gxz32
 */
public class OXFunction {

    static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}
    };
    static char currentPlayer = 'x';
    static int row, col;

    static void printProgramName() {
        System.out.println("Tic-Tac-Toe!");
    }

    static void printCurrentDescribe() {
        System.out.println("Current board layout:");
    }

    static void printCurrentBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println("");
            System.out.println("-------------");
        }
    }

    static void printTurn() {
        System.out.println("Player " + currentPlayer + "," + " enter an empty row and column to place your mark!");
    }

    static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                break;
            }

        }

    }

    static void changePlayer() {
        if (currentPlayer == 'x') {
            currentPlayer = 'o';
        } else {
            currentPlayer = 'x';
        }
    }

    static boolean isWin() {
        if (checkRowsForWin()) {
            return true;
        }
        if (checkColsForWin()) {
            return true;
        }
        if (checkDiagonalsForWin()) {
            return true;
        }   
        return false;
    }    
    
    static void showTie() {
        System.out.println("The game was a tie!");
    }

    static void showWin() {
        System.out.println(Character.toUpperCase(currentPlayer) + " Wins!");
    }

    static boolean checkRowsForWin() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkColsForWin() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDiagonalsForWin() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
    static boolean isBoardFull() {
        boolean isFull = true;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    isFull = false;
                }
            }
        }

        return isFull;
    }

    public static void main(String[] args) {
        printProgramName();
        while (true) {
            printCurrentDescribe();
            printCurrentBoard();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printCurrentDescribe();
                printCurrentBoard();
                showWin();
                break;
            } if(isBoardFull() && !isWin()) {
                printCurrentDescribe();
                printCurrentBoard();
                showTie();
                break;
            }
            changePlayer();
        }

    }
}
